// Теоретичні питання

//1.Описати своїми словами навіщо потрібні функції у програмуванні.
//Функции в прогрраммирование это глагол -  выполняют какое-то действия и сокращают
// написание кода. Функция может быть вызвана в любом месте.


//2.Описати своїми словами, навіщо у функцію передавати аргумент.
// Для того,что б функция работала с той информацией которую она получила в виде этого аргумента 

//3.Що таке оператор return та як він працює всередині функції?

//Return - это возвращение значения функции,результата ее работы. Работает так, что return возвращает результат 
// например обчисления какого-то и после return тело кода функции прекращает работу.


let num1 = +prompt ('Введи первое число');

let num2 = +prompt ('Введи второе число');


while (isNaN(num1) || isNaN(num2)) {
  num1 = parseFloat(prompt("Введи корректно первое число:"));
  num2 = parseFloat(prompt("Введи корректно второе число:"));
};

let operation = prompt("Введи математическую операцию (+, -, *, /):")

while (operation !== '+' && operation !== '-' && operation !== '*' && operation !== '/') {
  operation = prompt("Введи верную математическую операцию (+, -, *, /):")
};

function calculate(num1, num2, operation) {
  switch (operation) {
    case "+":
      return num1 + num2;
    case "-":
      return num1 - num2;
    case "*":
      return num1 * num2;
    case "/":
      if (num2 !== 0) {
        return num1 / num2;
      } else {
        return "Делить на 0 нельзя";
      }
    default:
      return "Неизвестная операция";
  }
}

const result = calculate(num1, num2, operation);
alert(`Результат: ${result}`);